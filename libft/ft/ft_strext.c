/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strext.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: juepee-m <juepee-m@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/02/14 18:58:46 by juepee-m          #+#    #+#             */
/*   Updated: 2019/09/19 22:01:45 by juepee-m         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

char			*ft_strext(char **dst, const char *src)
{
	char		*tmp;

	if (!(*dst) || !src)
		return (NULL);
	tmp = (*dst);
	if (!((*dst) = ft_strjoin((*dst), src)))
		return (NULL);
	ft_strdel(&tmp);
	return (*dst);
}
