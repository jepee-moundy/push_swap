/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   vector_print.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: juepee-m <juepee-m@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/11/22 11:22:16 by juepee-m          #+#    #+#             */
/*   Updated: 2019/09/19 22:02:06 by juepee-m         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "vector.h"

static void	print(void *e)
{
	ft_printf("%p\n", e);
}

void		vector_print(t_vector *v)
{
	vector_for_each(v, print);
}
