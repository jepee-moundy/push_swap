/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   lk_lst_init.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: juepee-m <juepee-m@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/05/20 13:53:45 by juepee-m          #+#    #+#             */
/*   Updated: 2019/09/19 22:01:57 by juepee-m         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

t_lklist			*lk_lst_init(void)
{
	t_lklist		*lst;

	lst = NULL;
	if ((lst = (t_lklist *)ft_memalloc(sizeof(t_lklist))))
	{
		lst->count = 0;
		lst->cur = NULL;
		lst->head = NULL;
		lst->tail = NULL;
	}
	return (lst);
}
