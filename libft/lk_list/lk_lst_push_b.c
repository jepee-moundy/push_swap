/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   lk_lst_push_b.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: juepee-m <juepee-m@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/05/19 19:23:41 by juepee-m          #+#    #+#             */
/*   Updated: 2019/09/19 22:01:59 by juepee-m         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

t_lknode			*lk_lst_push_b(t_lklist *lst, t_lknode *new)
{
	if (lst == NULL || new == NULL)
		return (NULL);
	new->header = lst;
	new->prev = lst->tail;
	if (lst->tail)
		lst->tail->next = new;
	lst->tail = new;
	if (lst->head == NULL)
		lst->head = new;
	if (lst->cur == NULL)
		lst->cur = new;
	lst->count++;
	return (new);
}
