/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   checker.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: juepee-m <juepee-m@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/05/07 15:11:07 by juepee-m          #+#    #+#             */
/*   Updated: 2019/09/23 17:41:40 by juepee-m         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "stack.h"

static int		check_sort(t_stack *a, t_stack *b)
{
	t_ste		*ste;
	long		i;

	if (b->size)
		return (0);
	i = 0;
	ste = a->p;
	while (i < a->size - 1)
	{
		if (ste->nbr > ste->next->nbr)
			return (0);
		ste = ste->next;
		i++;
	}
	return (1);
}

static int		process(t_stack *a, t_stack *b)
{
	char		*str;
	int			ret;

	ret = 1;
	while (ret && get_next_line(0, &str) > 0)
	{
		if (!(ret = st_exec(str, a, b, NULL)))
			ft_dprintf(2, "Error: invalid instruction (%s)\n", str);
		free(str);
	}
	return (ret);
}

int				main(int ac, char **av)
{
	t_stack		a;
	t_stack		b;
	int			c;

	c = 0;
	if (ac < 2)
		return (1);
	st_init(&a, &b);
	if (stack_load(&a, ac, av, &c) && c > 0)
	{
		if (process(&a, &b))
		{
			if (check_sort(&a, &b))
				ft_putendl("OK");
			else
				ft_putendl("KO");
		}
	}
	st_free(&a);
	st_free(&b);
	return (0);
}
