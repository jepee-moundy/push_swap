/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   values.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: juepee-m <juepee-m@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/08/22 12:51:48 by juepee-m          #+#    #+#             */
/*   Updated: 2019/09/22 00:55:27 by juepee-m         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "stack.h"

static long		*get_values(t_stack *s)
{
	t_ste		*ste;
	long		*tab;
	long		i;

	if ((tab = (long *)malloc(sizeof(long) * s->size)))
	{
		i = 0;
		ste = s->p;
		while (i < s->size)
		{
			tab[i] = ste->nbr;
			ste = ste->next;
			i++;
		}
	}
	return (tab);
}

static void		index_val(t_stack *s, t_ste *p, long *tab)
{
	long		i;

	i = 0;
	while (i <= s->size)
	{
		if (p->nbr == tab[i])
		{
			p->pos = &tab[i];
			return ;
		}
		i++;
	}
}

int				already_sorted(t_stack s)
{
	long		i;

	i = 0;
	while (i < s.size - 1)
	{
		if (s.p->next && s.p->pos > s.p->next->pos)
			return (0);
		s.p = s.p->next;
		i++;
	}
	return (1);
}

int				index_values(t_stack *s, long *tab)
{
	t_ste		*ste;
	long		i;

	i = 0;
	ste = s->p;
	while (i <= s->size)
	{
		index_val(s, ste, tab);
		ste = ste->next;
		i++;
	}
	return (already_sorted(*s) ? 1 : 0);
}

long			*get_sorted_values(t_stack *s)
{
	long		*tab;
	long		tmp;
	long		i;
	long		j;

	if ((tab = get_values(s)))
	{
		i = 0;
		while (i < s->size - 1)
		{
			j = 0;
			while (j < s->size - i - 1)
			{
				if (tab[j] > tab[j + 1])
				{
					tmp = tab[j];
					tab[j] = tab[j + 1];
					tab[j + 1] = tmp;
				}
				j++;
			}
			i++;
		}
	}
	return (tab);
}
