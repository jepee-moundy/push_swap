/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   push_swap.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: juepee-m <juepee-m@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/04/30 17:08:38 by juepee-m          #+#    #+#             */
/*   Updated: 2019/09/23 18:11:15 by juepee-m         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "stack.h"

static void		print_ops(t_op *op_lst)
{
	t_op		*ptr;

	while (op_lst)
	{
		ptr = op_lst;
		ft_putendl(op_lst->op);
		op_lst = op_lst->next;
		free(ptr->op);
		free(ptr);
	}
}

void			ft_launch(t_stack *a, t_stack *b, t_op **op_lst, long *s_val)
{
	if (index_values(a, s_val))
		return ;
	a->index = s_val;
	a->last = &s_val[a->size - 1];
	if (a->size <= 3)
		so_small(a, b, op_lst);
	if (a->size > 3 && a->size < 6)
		main_short(a, b, a->size, op_lst);
	if (a->size > 5)
		main_algo(a, b, a->size, op_lst);
	free(s_val);
}

int				main(int ac, char **av)
{
	long		*s_val;
	t_stack		a;
	t_stack		b;
	t_op		*op_lst;
	int			c;

	c = 0;
	if (!(op_lst = NULL) && ac < 2)
		return (0);
	st_init(&a, &b);
	if (stack_load(&a, ac, av, &c) && c > 1)
	{
		if ((s_val = get_sorted_values(&a)))
			ft_launch(&a, &b, &op_lst, s_val);
	}
	else
		return (0);
	optimize(&op_lst);
	print_ops(op_lst);
	st_free(&a);
	st_free(&b);
	return (0);
}
