/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   so_small.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: juepee-m <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/09/22 18:49:26 by juepee-m          #+#    #+#             */
/*   Updated: 2019/09/22 19:54:58 by juepee-m         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "stack.h"

void		so_small(t_stack *a, t_stack *b, t_op **op_lst)
{
	if (a->size == 2)
		st_exec("sa", a, b, op_lst);
	if (a->size == 3)
	{
		if (a->p->pos == a->index || a->p->pos == a->last)
			st_exec("ra", a, b, op_lst);
		if (a->p->next->pos == a->index && a->p->pos == a->index + 1)
			st_exec("sa", a, b, op_lst);
		if (a->p->prev->pos == a->index && a->p->pos == a->last)
			st_exec("sa", a, b, op_lst);
		if (a->p->prev->pos == a->index)
			st_exec("rra", a, b, op_lst);
	}
}
