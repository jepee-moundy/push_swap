/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main_algo2.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: juepee-m <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/09/17 18:59:40 by juepee-m          #+#    #+#             */
/*   Updated: 2019/09/18 16:14:46 by juepee-m         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "stack.h"

long			div_count(long size)
{
	long	count;

	count = 1;
	while (size > 3)
	{
		size = (size / 2);
		count++;
	}
	return (count);
}

int				rrewind(t_stack *a, t_stack *b, t_op **op_lst)
{
	while (a->p->prev->nbr >= *(a->index))
		st_exec("rra", a, b, op_lst);
	return (0);
}

long			*get_size_tab(long size)
{
	long	*tab;
	long	count_div;

	count_div = div_count(size);
	if (!(tab = (long *)malloc(sizeof(long) * count_div + 1)))
		return (0);
	tab[count_div--] = 0;
	while (size > 3)
	{
		tab[count_div--] = (size / 2) + (size % 2);
		size /= 2;
	}
	if (size > 0)
		tab[count_div] = size;
	else
		return (NULL);
	return (tab);
}
