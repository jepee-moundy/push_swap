/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main_short.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: juepee-m <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/09/18 18:32:26 by juepee-m          #+#    #+#             */
/*   Updated: 2019/09/19 19:17:15 by juepee-m         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "stack.h"

int			is_sorted(t_stack a)
{
	long		*p;

	p = a.index;
	while (p != a.last)
	{
		if (a.p->pos == a.index)
		{
			a.p = a.p->next;
			p = a.index;
			if (p == a.last)
				return (1);
		}
		else
			return (0);
	}
	return (0);
}

long		dif(t_stack a, long size)
{
	int		i;

	i = 0;
	while (i < size)
	{
		if (a.p->prev->pos == a.index)
			return (i);
		a.p = a.p->next;
		i++;
	}
	return (0);
}

int			ftn(t_stack *a, t_stack *b, t_op **op_lst)
{
	if (b->size > 0 && b->p->pos == a->index)
	{
		a->index++;
		st_exec("pa", a, b, op_lst);
		st_exec("ra", a, b, op_lst);
	}
	else if (b->size >= 2 && b->p->next->pos == a->index)
	{
		st_exec("sb", a, b, op_lst);
		st_exec("pa", a, b, op_lst);
		st_exec("ra", a, b, op_lst);
		a->index++;
	}
	else if (a->p->next->pos == a->index && (a->index += 1))
	{
		st_exec("sa", a, b, op_lst);
		st_exec("ra", a, b, op_lst);
	}
	else
		return (0);
	return (1);
}

void		get_first(t_stack *a, t_stack *b, long size, t_op **op_lst)
{
	int			diff;

	if (!(diff = dif(*a, size)))
		if (is_sorted(*a))
			return ;
	while (a->p->prev->pos != a->index)
		diff <= (size / 2) ? st_exec("ra", a, b, op_lst)
			: st_exec("rra", a, b, op_lst);
	a->index++;
	while (1)
	{
		if (!ftn(a, b, op_lst))
		{
			(a->p->pos == a->index && (a->index += 1)) ?
				st_exec("ra", a, b, op_lst) : st_exec("pb", a, b, op_lst);
		}
		if (a->index == a->last)
		{
			if (b->size > 0)
				st_exec("pa", a, b, op_lst);
			st_exec("ra", a, b, op_lst);
			return ;
		}
	}
}

int			main_short(t_stack *a, t_stack *b, long size, t_op **op_lst)
{
	get_first(a, b, size, op_lst);
	return (0);
}
