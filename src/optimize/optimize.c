/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   optimize.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: juepee-m <juepee-m@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/07/11 19:25:36 by juepee-m          #+#    #+#             */
/*   Updated: 2019/09/19 22:02:09 by juepee-m         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "stack.h"

static int		opti_comp(t_op *op, char *r1, char *r2)
{
	if (!ft_strcmp(op->op, r1) && \
			!ft_strcmp(op->next->op, r2))
		return (1);
	if (!ft_strcmp(op->op, r2) && \
			!ft_strcmp(op->next->op, r1))
		return (1);
	return (0);
}

static t_op		*opti_push(t_op *op_lst)
{
	if (op_lst->next)
		if (opti_comp(op_lst, "pa", "pb"))
			return (op_lst->next->next);
	return (NULL);
}

static t_op		*opti_rotate(t_op *op_lst)
{
	if (op_lst->next)
	{
		if (opti_comp(op_lst, "ra", "rra"))
			return (op_lst->next->next);
		if (opti_comp(op_lst, "rb", "rrb"))
			return (op_lst->next->next);
	}
	return (NULL);
}

static t_op		*opti(t_op *op_lst)
{
	t_op		*ret;
	t_op		*del;
	int			ctn;

	ctn = (op_lst ? 1 : 0);
	while (ctn)
	{
		ret = NULL;
		if (op_lst->op[0] == 'p')
			ret = opti_push(op_lst);
		else if (op_lst->op[0] == 'r')
			ret = opti_rotate(op_lst);
		if (ret)
			return (ret);
		ret = opti(op_lst->next);
		del = op_lst;
		while ((del = del->next) && del != ret)
		{
			free(del->op);
			free(del);
		}
		ctn = (!ret || ret == op_lst->next ? 0 : 1);
		op_lst->next = ret;
	}
	return (op_lst);
}

void			optimize(t_op **op_lst)
{
	t_op		*first;
	t_op		*ptr;
	t_op		*tmp;

	ptr = *op_lst;
	first = opti(*op_lst);
	while (ptr != first)
	{
		tmp = ptr->next;
		free(ptr->op);
		free(ptr);
		ptr = tmp;
	}
	*op_lst = first;
}
