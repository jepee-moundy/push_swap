/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   op_swap.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: juepee-m <juepee-m@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/05/06 16:22:54 by juepee-m          #+#    #+#             */
/*   Updated: 2019/09/19 22:02:09 by juepee-m         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "stack.h"

static int		swap(t_stack *s)
{
	long		*pos_tmp;
	int			tmp;

	if (s->size >= 2)
	{
		pos_tmp = s->p->pos;
		tmp = s->p->nbr;
		s->p->nbr = s->p->next->nbr;
		s->p->pos = s->p->next->pos;
		s->p->next->nbr = tmp;
		s->p->next->pos = pos_tmp;
	}
	return (1);
}

int				op_swap(char sel, t_stack *a, t_stack *b)
{
	int		ret;

	ret = 0;
	if (sel == 'a' || sel == 's')
		ret = swap(a);
	if (sel == 'b' || sel == 's')
		ret = swap(b);
	return (ret);
}
