/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   op_revrotate.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: juepee-m <juepee-m@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/05/06 18:07:27 by juepee-m          #+#    #+#             */
/*   Updated: 2019/09/19 22:02:09 by juepee-m         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "stack.h"

static int		revrotate(t_stack *s)
{
	if (s->size > 1)
		s->p = s->p->prev;
	return (1);
}

int				op_revrotate(char sel, t_stack *a, t_stack *b)
{
	int			ret;

	ret = 0;
	if (sel == 'a' || sel == 'r')
		ret = revrotate(a);
	if (sel == 'b' || sel == 'r')
		ret = revrotate(b);
	return (ret);
}
