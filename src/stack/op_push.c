/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   op_push.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: juepee-m <juepee-m@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/05/06 18:46:43 by juepee-m          #+#    #+#             */
/*   Updated: 2019/09/19 22:02:09 by juepee-m         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "stack.h"

static int		push(t_stack *dst, t_stack *src)
{
	if (src->size)
		st_push(dst, st_pop(src));
	return (1);
}

int				op_push(char sel, t_stack *a, t_stack *b)
{
	if (sel == 'a')
		return (push(a, b));
	if (sel == 'b')
		return (push(b, a));
	return (0);
}
