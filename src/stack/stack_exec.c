/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   stack_exec.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: juepee-m <juepee-m@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/05/06 15:39:22 by juepee-m          #+#    #+#             */
/*   Updated: 2019/09/22 02:54:48 by juepee-m         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "stack.h"

static void		log_op(char *op, t_op **op_lst)
{
	t_op		*n_op;

	if (op_lst)
	{
		if ((n_op = malloc(sizeof(t_op))))
		{
			n_op->next = NULL;
			n_op->op = ft_strdup(op);
			if (*op_lst)
				(*op_lst)->last->next = n_op;
			else
				(*op_lst) = n_op;
			(*op_lst)->last = n_op;
		}
	}
}

int				st_exec(char *op, t_stack *a, t_stack *b, t_op **op_lst)
{
	int			op_len;
	int			ret;

	if (a->index > a->last)
		return (0);
	op_len = ft_strlen(op);
	ret = 0;
	if (op_len == 2)
	{
		if (*op == 's')
			ret = op_swap(op[1], a, b);
		else if (*op == 'p')
			ret = op_push(op[1], a, b);
		else if (*op == 'r')
			ret = op_rotate(op[1], a, b);
	}
	else if (op_len == 3 && !ft_strncmp(op, "rr", 2))
		ret = op_revrotate(op[2], a, b);
	if (ret)
		log_op(op, op_lst);
	return (ret);
}
