/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   stack.c                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: juepee-m <juepee-m@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/04/30 13:37:38 by juepee-m          #+#    #+#             */
/*   Updated: 2019/09/22 20:17:31 by juepee-m         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "stack.h"

void			st_init(t_stack *a, t_stack *b)
{
	a->size = 0;
	a->sort = 0;
	a->index = 0;
	a->p = NULL;
	b->size = 0;
	b->p = NULL;
}

t_ste			*st_enew(long nbr)
{
	t_ste		*enew;

	if ((enew = (t_ste *)malloc(sizeof(t_ste))))
	{
		enew->nbr = nbr;
		enew->next = NULL;
		enew->prev = NULL;
	}
	return (enew);
}

void			st_push(t_stack *s, t_ste *ste)
{
	t_ste		*tmp;

	ste->next = s->p;
	if (s->size == 1)
	{
		s->p->prev = ste;
		s->p->next = ste;
		ste->prev = s->p;
	}
	else if (s->size > 1)
	{
		tmp = s->p->prev;
		s->p->prev = ste;
		ste->next = s->p;
		ste->prev = tmp;
		tmp->next = ste;
	}
	s->size++;
	s->p = ste;
}

t_ste			*st_pop(t_stack *s)
{
	t_ste		*epop;

	if ((epop = s->p))
	{
		s->p = epop->next;
		if (s->size == 2)
		{
			s->p->next = NULL;
			s->p->prev = NULL;
		}
		else if (s->size > 2)
		{
			epop->prev->next = epop->next;
			epop->next->prev = epop->prev;
		}
		epop->next = NULL;
		epop->prev = NULL;
		s->size--;
	}
	return (epop);
}

void			st_free(t_stack *s)
{
	while (s->size)
		free(st_pop(s));
}
