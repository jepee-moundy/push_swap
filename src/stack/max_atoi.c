/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   max_atoi.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: juepee-m <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/09/22 21:35:35 by juepee-m          #+#    #+#             */
/*   Updated: 2019/09/23 16:15:40 by juepee-m         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "stack.h"

static long		read_nbr(char const *str, long nb)
{
	long	n;

	n = 0;
	while (ft_isdigit(*str))
	{
		n = n * 10 + ((*str) - '0');
		str++;
	}
	return ((n - nb) > INT_MAX ? 0 : 1);
}

long			max_atoi(char const *str)
{
	long	ret;

	ret = 0;
	while (*str == ' ' || (*str >= 9 && *str <= 13))
		str++;
	if (*str == '-' && ft_isdigit(*(str + 1)))
		return (!!(ret = read_nbr((str + 1), 1)));
	if (*str == '+' && ft_isdigit(*(str + 1)))
		return (!!(ret = read_nbr(str + 1, 0)));
	if (ft_isdigit(*str))
		return (!!(ret = read_nbr(str, 0)));
	return (0);
}
