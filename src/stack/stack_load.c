/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   stack_load.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: juepee-m <juepee-m@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/04/30 15:55:18 by juepee-m          #+#    #+#             */
/*   Updated: 2019/09/23 17:21:05 by juepee-m         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "stack.h"

static long		check_av(char *av)
{
	char		*s;

	s = av;
	if (*av == '-' || *av == '+')
		av++;
	while (*av)
	{
		if (!ft_isdigit(*av))
		{
			ft_dprintf(2, "Error: not a number (%s)\n", s);
			return (0);
		}
		av++;
	}
	return (1);
}

static int		check_dupe(t_stack *s, int nbr)
{
	t_ste		*ste;
	long		i;

	if ((ste = s->p))
	{
		i = 0;
		while (i < s->size)
		{
			if (ste->nbr == nbr)
			{
				ft_dprintf(2, "Error: duplicated number (%d)\n", ste->nbr);
				return (0);
			}
			ste = ste->next;
			i++;
		}
	}
	return (1);
}

static long		load_av(t_stack *s, int ac, char **av)
{
	t_ste		*enew;
	int			nbr;
	long		ret;

	ret = 1;
	while (ret && ac >= 0)
	{
		if ((ret = (check_av(av[ac]))))
		{
			if ((ret = max_atoi(av[ac])) == 0)
			{
				ft_dprintf(2, "Error: number is out of limit\n");
				return (0);
			}
			nbr = ft_atoi(av[ac]);
			if (check_dupe(s, nbr) && (enew = st_enew(nbr)))
				st_push(s, enew);
			else
				ret = 0;
		}
		ac--;
	}
	return (ret);
}

int				stack_load(t_stack *s, int ac, char **av, int *c)
{
	char		**tab;
	char		**ptr;
	int			ret;
	int			i;

	ret = 1;
	while (ret && --ac)
	{
		if ((tab = ft_strsplit(av[ac], ' ')))
		{
			ptr = tab;
			i = -1;
			while (tab[++i])
				*c += 1;
			ret = load_av(s, i - 1, tab);
			ft_freetab(ptr);
		}
		else
			ret = 0;
	}
	return (ret);
}
