/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   stack.h                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: juepee-m <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/09/19 21:12:57 by juepee-m          #+#    #+#             */
/*   Updated: 2019/09/23 16:44:14 by juepee-m         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef STACK_H
# define STACK_H
# include "libft.h"

# include <stdio.h>
# include <limits.h>

typedef struct			s_stack
{
	long				size;
	long				sort;
	long				*index;
	long				*last;
	struct s_ste		*p;
}						t_stack;

typedef struct			s_ste
{
	long				nbr;
	long				*pos;
	struct s_ste		*next;
	struct s_ste		*prev;
}						t_ste;

typedef struct			s_op
{
	char				*op;
	struct s_op			*last;
	struct s_op			*next;
}						t_op;

void					st_init(t_stack *a, t_stack *b);
t_ste					*st_enew(long nbr);
void					st_push(t_stack *s, t_ste *enew);
t_ste					*st_pop(t_stack *s);
void					st_free(t_stack *s);
void					st_print(t_stack *s);

int						st_exec(char *op, t_stack *a, t_stack *b, \
									t_op **op_lst);

int						stack_load(t_stack *s, int ac, char **av, int *c);

int						op_push(char sel, t_stack *a, t_stack *b);
int						op_swap(char sel, t_stack *a, t_stack *b);
int						op_rotate(char sel, t_stack *a, t_stack *b);
int						op_revrotate(char sel, t_stack *a, t_stack *b);

void					optimize(t_op **op_lst);

void					get_first(t_stack *a, t_stack *b, long size, \
									t_op **op_lst);
int						main_algo(t_stack *a, t_stack *b, long size, \
									t_op **op_lst);
int						main_short(t_stack *a, t_stack *b, long size, \
									t_op **op_lst);
long					*get_size_tab(long size);
int						rrewind(t_stack *a, t_stack *b, t_op **op_lst);
int						add_list(t_stack *a, int size);
void					ping_bpush(t_stack *a, t_stack *b, t_op **op_lst);
void					check_bfirst(t_stack *a, t_stack *b, t_op **op_lst);
void					size_asort(t_stack *a, t_stack *b, long size, \
									t_op **op_lst);
int						size_bsort(t_stack *a, t_stack *b, long size, \
									t_op **op_lst);
void					get_end(t_stack *a, t_stack *b, int size);
void					so_small(t_stack *a, t_stack *b, t_op **op_lst);
long					max_atoi(char const *str);

int						index_values(t_stack *s, long *tab);
long					*get_sorted_values(t_stack *s);
#endif
